package com.victorlapin.lock;

import android.app.Activity;
import android.os.Bundle;

import java.io.IOException;

public class LockActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Runtime.getRuntime().exec("su -c input keyevent 26");
        } catch (IOException ignore) { }
        finish();
    }
}
